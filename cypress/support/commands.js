Cypress.Commands.add('registrationPage', () => {
    cy.visit("");
    cy.get('a[class="login"]').click();
})

Cypress.Commands.add('contactUsPage', () => {
    cy.visit("");
    cy.get('a[title="Napište nám"]').first().click();
})

Cypress.Commands.add('loginIntoApp', () => {
    cy.visit("");
    cy.get('a[class="login"]').click();
    cy.get('button[id="opc_show_login"]').click();
    cy.get('input[id="txt_login_email"]').type("testingcypress@gmail.com");
    cy.get('input[id="txt_login_password"]').type("testingcypress");
    cy.get('button[id="btn_login"]').click();

    //testingcypress@gmail.com - heslo stejné
})


// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })