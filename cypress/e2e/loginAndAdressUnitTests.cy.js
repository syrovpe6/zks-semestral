describe("login and adress tests", () => {
    const firstName = "Jan";
    const lastName = "Novák";
    const deliverAddress = "Na Kotouči 5";
    const psc = "108 00";
    const city = "Praha";
    const phoneNumber = "+420605051913";
    const deliveryAlias = "DefaultAdress";

    beforeEach(() => {
        cy.loginIntoApp();
    })

    it("logged into correct account", () => {
        expect(cy.contains("Testing Testing")).to.exist;
    })

    it("logout test", () => {
        cy.get('a[title="Odhlásit se"]').click();
        expect(cy.contains("Přihlásit se")).to.exist;
    })

    it("addAdress", () => {
        cy.get('a[title="Zobrazit můj zákaznický účet"]').click();
        cy.get('a[title="Moje adresy"]').click();

        cy.get('div[id="panel_address_delivery"]').click();
        cy.get('div[id="panel_address_delivery"]').within(() => {
            cy.get('div[id="address_card_new_content"]').click();
        })


        cy.get('input[id="delivery_firstname"]').type(firstName);
        cy.get('input[id="delivery_lastname"]').type(lastName);
        cy.get('input[id="delivery_address1"]').type(deliverAddress);
        cy.get('input[id="delivery_postcode"]').type(psc);
        cy.get('input[id="delivery_city"]').type(city);
        cy.get('input[id="delivery_phone_mobile"]').type(phoneNumber);
        cy.get('input[id="delivery_alias"]').type(deliveryAlias);

        cy.get('button[id="btn_update_address_delivery"]').click();


        expect(cy.contains(deliveryAlias)).to.exist;
    })

    it("removeAdress", () => {
        cy.get('a[title="Zobrazit můj zákaznický účet"]').click();
        cy.get('a[title="Moje adresy"]').click();

        cy.get('div[id="panel_address_delivery"]').click();

        cy.contains(deliveryAlias).then(() => {
            cy.contains(deliveryAlias).parent().parent().contains("Delete").click({ force: true });
        });

        cy.contains(deliveryAlias).should('not.exist');
    })
})