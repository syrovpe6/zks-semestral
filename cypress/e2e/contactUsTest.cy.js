import contactUsData from "../fixtures/contactUsData.json";
import contactUsPage from "../pageObjects/contactUsPage";

contactUsData.forEach((contactData => {
  describe('registration test ' + contactData.testCase, () => {
    it('fill contact form', () => {
      cy.contactUsPage();
      
      if(contactData.message.length != 0)
      {
        contactUsPage.typeIntoMessage(contactData.message);
      }

      contactUsPage.typeIntoEmail(contactData.email);

      if(contactData.captcha)
      {
        contactUsPage.checkCaptcha;
      }

      contactUsPage.subjectSelect(contactData.subject);

      contactUsPage.clickSubmit();

      if(contactData.result)
      {
        //expect(cy.get('p[class="alert alert-success"')).to.exist;
        // :( captcha
        
        expect(cy.contains("Please validate the captcha field before submitting your request")).to.exist;
      }
      else
      {
        expect(cy.get('div[class="alert alert-danger"]')).to.exist;
      }
    })
  })
}))

//testingcypress@gmail.com - heslo stejné