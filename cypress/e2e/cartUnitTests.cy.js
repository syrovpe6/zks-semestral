
describe("cart unit tests", () => {


    beforeEach(() => {
        cy.visit("");
    })

    it("cart should be empty on start", () => {
        cy.get('a[title="Zobrazit můj nákupní košík"]').click();
        expect(cy.contains("Váš nákupní košík je prázdný.")).to.exist;
    })

    it("adding one product", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();
        cy.get('a[data-id-product="1"][title="Přidat do košíku"]').click();
        cy.get('span[title="Zavřít okno"]').click();
        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', '1');
    })

    it("adding one product multiple times", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        for (let i = 0; i < 3; i++) {
            cy.get('a[data-id-product="1"][title="Přidat do košíku"]').click();
            cy.get('span[title="Zavřít okno"]').click();
        }

        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', '3');
    })

    it("adding multiple different products", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[data-id-product="1"][title="Přidat do košíku"]').click();
        cy.get('span[title="Zavřít okno"]').click();

        cy.get('a[data-id-product="2"][title="Přidat do košíku"]').click();
        cy.get('span[title="Zavřít okno"]').click();

        cy.get('a[data-id-product="4"][title="Přidat do košíku"]').click();
        cy.get('span[title="Zavřít okno"]').click();


        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', '3');
    })

    it("adding one product in larger quantity", () => {
        const itemQuantity = 15;

        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        //cy.get('a[data-id-product="1"]').siblings().within(() => {cy.get('a[title="Zobrazit"]').click();})

        cy.get('a[title="Zobrazit"]').first().click();
        cy.get('input[id="quantity_wanted"]').clear().type(itemQuantity);
        cy.get('#add_to_cart > button').click();
        cy.get('span[title="Zavřít okno"]').click();

        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', itemQuantity.toString()    );
    })

    it("adding one product in larger quantity multiple times", () => {
        const itemQuantity1 = 15;

        const itemQuantity2 = 25;

        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[title="Zobrazit"]').first().click();

        cy.get('input[id="quantity_wanted"]').clear().type(itemQuantity1);
        cy.get('#add_to_cart > button').click();
        cy.get('span[title="Zavřít okno"]').click();

        cy.get('input[id="quantity_wanted"]').clear().type(itemQuantity2);
        cy.get('#add_to_cart > button').click();
        cy.get('span[title="Zavřít okno"]').click();

        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', (itemQuantity1 + itemQuantity2).toString());
    })

    it("adding one product in maximum quantity", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[title="Zobrazit"]').first().click();

        cy.get('span[id="quantityAvailable"]').invoke('text').then((text) => {
            cy.get('input[id="quantity_wanted"]').clear().type(text.trim());
            cy.get('#add_to_cart > button').click();
            cy.get('span[title="Zavřít okno"]').click();
            
        })

        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', text.trim());
    })

    it("adding one product in more than maximum quantity", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[title="Zobrazit"]').first().click();

        cy.get('span[id="quantityAvailable"]').invoke('text').then((text) => {
            cy.get('input[id="quantity_wanted"]').clear().type(Number(text.trim()) + 1);
            cy.get('#add_to_cart > button').click();
        })

        expect(cy.contains("Na skladě není dostatečné množství zboží.")).to.exist;
    })

    it("adding more quantities multiple times than in stock", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[title="Zobrazit"]').first().click();

        cy.get('span[id="quantityAvailable"]').invoke('text').then((text) => {
            cy.get('input[id="quantity_wanted"]').clear().type(Number(text.trim()));
            cy.get('#add_to_cart > button').click();
            cy.get('span[title="Zavřít okno"]').click();

            cy.get('input[id="quantity_wanted"]').clear().type(1);
            cy.get('#add_to_cart > button').click();
        })

        expect(cy.contains("Na skladě není dostatečné množství zboží.")).to.exist;
    })

    it("adding mulitple procucts using form", () => {
        const itemQuantity = 10;

        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[title="Zobrazit"]').first().click();

        cy.get('span[id="quantityAvailable"]').invoke('text').then((text) => {
            cy.get('input[id="quantity_wanted"]').clear().type(itemQuantity);
            cy.get('#add_to_cart > button').click();
            cy.get('span[title="Zavřít okno"]').click();
        })

        cy.get('.sf-menu > :nth-child(2) > [title="Dresses"]').click();

        cy.get('a[title="Zobrazit"]').first().click();
        cy.get('span[id="quantityAvailable"]').invoke('text').then((text) => {
            cy.get('input[id="quantity_wanted"]').clear().type(itemQuantity);
            cy.get('#add_to_cart > button').click();
            cy.get('span[title="Zavřít okno"]').click();
        })

        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', (2 * itemQuantity).toString());
    })

    it("adding and removing an item", () => {
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();
        cy.get('a[data-id-product="1"][title="Přidat do košíku"]').click();
        cy.get('span[title="Zavřít okno"]').click();
        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', '1');

        cy.get('#header > div:nth-child(3) > div > div > div:nth-child(3) > div > div > div > div > dl > dt > span > a').click({force: true});


        cy.get('a[title="Zobrazit můj nákupní košík"] > span').should('contain', "(prázdný)");
    })
})