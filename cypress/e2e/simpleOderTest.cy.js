import orderPage from "../pageObjects/orderPage";

describe("Path based test of simple order", () => {

    beforeEach(() => {
        cy.visit("");
        cy.get('.sf-menu > :nth-child(1) > [title="Women"]').click();

        cy.get('a[data-id-product="1"][title="Přidat do košíku"]').click();
        cy.wait(500)
        cy.get('[title="Objednat"]').click();

    })

    afterEach(() => {
        cy.get('a[class="logout"]').click();   
    })

    it("Vyber Zbozi - Vyzvednuti na prodejne - Dobirka", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectInStorePickupOption();
        orderPage.selectCashOnDeliveryOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })

    it("Vyber Zbozi - Doruceni - Dobirka", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectDeliveryOption();
        orderPage.selectCashOnDeliveryOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })

    it("Vyber Zbozi - Vyzvednuti na prodejne - Sek", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectInStorePickupOption();
        orderPage.selectChequeOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })

    it("Vyber Zbozi - Vyzvednuti na prodejne - Prevod", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectInStorePickupOption();
        orderPage.selectBankWireOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })

    it("Vyber Zbozi - Doruceni - Sek", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectDeliveryOption();
        orderPage.selectChequeOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })

    it("Vyber Zbozi - Doruceni - Prevod", () => {
        orderPage.login();

        cy.reload();

        orderPage.selectDeliveryOption();
        orderPage.selectBankWireOption();
        orderPage.placeOrder(); 
        orderPage.confirmOrder();

        expect(cy.contains("Potvrzení objednávky")).to.exist;
    })
})