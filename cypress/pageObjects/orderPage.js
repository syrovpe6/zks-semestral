class orderPage {
    elements = {
        inStorePickupOption: () => cy.get('input[type="radio"][value="1,"]'),
        deliveryOption: () => cy.get('input[type="radio"][value="2,"]'),

        bankWireOption: () => cy.get('input[type="radio"][value="bankwire"  ]'),
        chequeOption: () => cy.get('input[type="radio"][value="cheque"]'),
        cashOnDeliveryOption: () => cy.get('input[type="radio"][value="cashondelivery"]'),

        placeOrderButton: () => cy.get('button[id="btn_place_order"]'),
        confirmOrderButton: () => cy.get('form > p > button'),
    }

    selectInStorePickupOption() {
        this.elements.inStorePickupOption().click();
    }

    selectDeliveryOption() {
        this.elements.deliveryOption().click();
    }

    selectBankWireOption() {
        this.elements.bankWireOption().click();
    }

    selectChequeOption() {
        this.elements.chequeOption().click();
    }

    selectCashOnDeliveryOption() {
        this.elements.cashOnDeliveryOption().click();
    }

    placeOrder() {
        this.elements.placeOrderButton().click();
    }

    confirmOrder() {
        this.elements.confirmOrderButton().click();
    }

    login() {
        cy.get('button[id="opc_show_login"]').click();
        cy.get('input[id="txt_login_email"]').type("testingcypress@gmail.com");
        cy.get('input[id="txt_login_password"]').type("testingcypress");
        cy.get('button[id="btn_login"]').click();
    }
}

module.exports = new orderPage();