class ContactUsPage {
    elements = {
       emailInput: () => cy.get('input[id="email"]'),
       messageInput: () => cy.get('textarea[id="message"]'),
       captchaCheckBox: () => cy.get('div[class="recaptcha-checkbox-border"]'),
       subjectSelect: () => cy.get('select[id="id_contact"]'),
       submitButton: () => cy.get('button[id="submitMessage"]')
    }

    typeIntoEmail(text)
    {
        this.elements.emailInput().type(text);
    }

    typeIntoMessage(text)
    {
        this.elements.messageInput().type(text);
    }

    clickMessage()
    {
        this.elements.messageInput().click();
    }

    checkCaptcha()
    {
        this.elements.checkCaptcha().click();
    }

    subjectSelect(subject)
    {
        this.elements.subjectSelect().select(subject);
    }

    clickSubmit()
    {
        this.elements.submitButton().click();
    }
}

module.exports = new ContactUsPage();

//https://www.browserstack.com/guide/cypress-page-object-model